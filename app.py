from flask import Flask, request, render_template, send_file, jsonify
from kedro.framework.session import KedroSession
from kedro.framework.startup import bootstrap_project
from pathlib import Path
import os

app = Flask(__name__)

# Initialize Kedro project
project_path = Path(__file__).resolve().parent
metadata = bootstrap_project(project_path)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/predict', methods=['GET', 'POST'])
def predict():
    if request.method == 'POST':
        # Récupération de l'image téléchargée
        if 'image' not in request.files:
            return jsonify({'error': 'No image part'}), 400

        file = request.files['image']

        if file.filename == '':
            return jsonify({'error': 'No selected file'}), 400

        if file:
            # Sauvegarder l'image téléchargée sous un nom fixe
            upload_folder = 'uploads'
            if not os.path.exists(upload_folder):
                os.makedirs(upload_folder)
            image_path = os.path.join(upload_folder, 'input_image.jpg')
            file.save(image_path)

            # Définir le chemin de sauvegarde pour l'image annotée
            save_path = os.path.join(upload_folder, 'output_image.jpg')

            # Exécution du pipeline Kedro
            with KedroSession.create(project_path) as session:
                context = session.load_context()
                catalog = context.catalog
                result = session.run(pipeline_name="pipeline_prediction")  # Exécuter le pipeline 'pipeline_prediction'

            # Retourner l'image annotée
            return send_file(save_path, mimetype='image/jpeg')

    # Si la méthode est GET, rendre le formulaire de prédiction
    return render_template('predict.html')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port = 8888)
