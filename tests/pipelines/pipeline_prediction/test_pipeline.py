import pytest
from unittest.mock import patch, MagicMock
from pathlib import Path
import sys
import warnings

# Ajouter le chemin src au sys.path
sys.path.append(str(Path(__file__).resolve().parents[3] / 'src'))

# Filtre des avertissements
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=PendingDeprecationWarning)
warnings.filterwarnings("ignore", category=ResourceWarning)


from mlops.pipelines.pipeline_prediction.nodes import load_model, predict

@pytest.fixture
def weights_path(tmp_path):
    return tmp_path / "dummy_weights.pt"

@pytest.fixture
def image_path(tmp_path):
    return tmp_path / "dummy_image.jpg"

@patch('torch.hub.load', return_value=MagicMock())
def test_predict(mock_load, image_path, weights_path):
    # Mock the model
    mock_model = MagicMock()
    mock_load.return_value = mock_model
    
    # Mock the prediction results
    mock_results = MagicMock()
    mock_model.return_value = mock_results
    
    # Load the model
    model = load_model(weights_path)
    
    # Perform prediction
    results = predict(model, image_path)
    
    # Verify the predictions
    mock_load.assert_called_once_with('ultralytics/yolov5', 'custom', path=weights_path)
    mock_model.assert_called_once_with(image_path)
    assert results == mock_results
