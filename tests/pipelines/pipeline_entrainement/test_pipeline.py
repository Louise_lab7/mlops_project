import pytest
from unittest.mock import patch, MagicMock
import torch
import sys
import warnings
from pathlib import Path

# Ajouter le chemin src au sys.path
sys.path.append(str(Path(__file__).resolve().parents[3] / 'src'))

from mlops.pipelines.pipeline_entrainement.nodes import train_yolov5, save_model_weights

# Filtre des avertissements
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=PendingDeprecationWarning)
warnings.filterwarnings("ignore", category=ResourceWarning)

# Mock de la fonction check_dataset de YOLOv5 pour éviter l'erreur de dataset manquant
@patch('yolov5.utils.general.check_dataset', return_value={'train': 'dummy_train_path', 'val': 'dummy_val_path'})
# Mock de la fonction train.run de YOLOv5
@patch('yolov5.train.run', return_value={'model': MagicMock()})
# Mock de os.path.exists pour les chemins de fichiers
@patch('os.path.exists', return_value=True)
# Mock de torch.save pour éviter l'écriture de fichiers
@patch('torch.save')
# Mock de la fonction isfile pour les fichiers de dataset
@patch('yolov5.utils.general.isfile', return_value=True)
# Mock de la fonction isdir pour les répertoires de dataset
@patch('yolov5.utils.general.isdir', return_value=True)
# Mock de os.listdir pour éviter les erreurs de lecture de répertoire
@patch('os.listdir', return_value=['dummy_image.jpg'])
# Mock de la fonction load_images_and_labels de YOLOv5 pour éviter les erreurs de chargement d'images
@patch('yolov5.utils.dataloaders.load_images_and_labels', return_value=([], [], []))
def test_train_yolov5(mock_torch_save, mock_train_run, mock_check_dataset):
    # Appeler la fonction d'entraînement
    results = train_yolov5()

    # Vérifier que la fonction check_dataset a été appelée
    assert mock_check_dataset.called

    # Vérifier que la fonction train.run a été appelée
    assert mock_train_run.called

    # Vérifier que les résultats contiennent un modèle
    assert 'model' in results

    # Vérifier que torch.save n'a pas été appelée directement dans ce test
    assert not mock_torch_save.called

# Mock de torch.save pour éviter l'écriture de fichiers
@patch('torch.save')
def test_save_model_weights(mock_torch_save):
    mock_model = MagicMock()
    mock_results = {'model': mock_model}
    save_path = 'dummy_path.pt'

    # Appeler la fonction pour sauvegarder les poids du modèle
    save_model_weights(mock_results, save_path)

    # Vérifier que torch.save a été appelée avec les bons paramètres
    mock_model.state_dict.assert_called_once()
    mock_torch_save.assert_called_once_with(mock_model.state_dict(), save_path)

if __name__ == "__main__":
    pytest.main()
