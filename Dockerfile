FROM python:3.9-slim

WORKDIR /app

COPY requirements.txt /app

# Afficher le contenu de requirements.txt pour le débogage
RUN cat /app/requirements.txt

# Installer les dépendances système pour OpenCV
RUN apt-get update && apt-get install -y \
    libgl1-mesa-glx \
    libglib2.0-0

# Mettre à jour pip et installer les dépendances
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Exposez le port sur lequel l'application Flask écoute
EXPOSE 8888

# Copiez le reste de l'application
COPY . /app

CMD ["python", "app.py"]