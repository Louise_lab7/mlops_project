"""Project pipelines."""
from typing import Dict

from kedro.framework.project import find_pipelines
from kedro.pipeline import Pipeline
from mlops.pipelines import pipeline_prediction 


def register_pipelines() -> Dict[str, Pipeline]:
    """Register the project's pipelines.

    Returns:
        A mapping from pipeline names to ``Pipeline`` objects.
    """
    # Créez un dictionnaire avec uniquement le pipeline_prediction
    pipelines = {
        "pipeline_prediction": pipeline_prediction.create_pipeline(),
        "__default__": pipeline_prediction.create_pipeline()
    }
    
    return pipelines