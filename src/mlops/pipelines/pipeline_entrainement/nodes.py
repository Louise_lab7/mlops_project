"""
This is a boilerplate pipeline 'pipeline_entrainement'
generated using Kedro 0.19.6
"""

import os
import numpy as np
from PIL import Image
from pathlib import Path
import yaml
from yolov5 import train
import torch
import torch.multiprocessing as mp
import warnings




# Supprimer tous les avertissements
warnings.filterwarnings("ignore")

# Assurez-vous que le mode de démarrage des processus est 'spawn'
mp.set_start_method('spawn', force=True)

# Assurez-vous que PyTorch peut utiliser le GPU
print(f"Is CUDA available? {torch.cuda.is_available()}")
if torch.cuda.is_available():
    print(f"CUDA Device Name: {torch.cuda.get_device_name(0)}")

# Vérifiez la disponibilité de MPS (Metal Performance Shaders)
mps_available = torch.backends.mps.is_available()
print(f"Is MPS available? {mps_available}")

device = 'cuda' if torch.cuda.is_available() else 'mps' if mps_available else 'cpu'
print(f"Using device: {device}")


def save_model_weights(results, save_path):
    # Récupérer le modèle entraîné depuis les résultats
    model = results['model']

    # Sauvegarder les poids du modèle
    torch.save(model.state_dict(), save_path)

def train_yolov5():
    # Utilisation de autocast uniquement si CUDA est disponible
    if torch.cuda.is_available():
        with torch.cuda.amp.autocast():
            results = train.run(
                data='data/data.yaml', 
                imgsz=384, 
                epochs=1, 
                batch=8, 
                workers=8, 
                weights='yolov5s.pt', 
                plots=False, 
                device=device
            )
    else:
        results = train.run(
            data='data/data.yaml', 
            imgsz=384, 
            epochs=1, 
            batch=2, 
            workers=8, 
            weights='yolov5s.pt', 
            plots=False, 
            device=device
        )
    return results

# Appel de la fonction pour commencer l'entraînement
train_yolov5()
