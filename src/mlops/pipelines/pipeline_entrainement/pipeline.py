"""
This is a boilerplate pipeline 'pipeline_entrainement'
generated using Kedro 0.19.6
"""


from kedro.pipeline import Pipeline, node
from .nodes import train_yolov5, save_model_weights

def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=train_yolov5,
                inputs=[],
                outputs="yolov5_model",
                name="train_yolov5_model"
            ),
            node(
                func=save_model_weights,
                inputs="yolov5_model",
                outputs=None,
                name="save_yolov5_weights",
                tags=["save_weights"],
                save_path="runs/train/best.pt"  # Chemin vers les poids du modèle YOLOv5 entraîné
            ),
        ]
    )
    
