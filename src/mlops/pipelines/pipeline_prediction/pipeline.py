from kedro.pipeline import Pipeline, node
from .nodes import load_model, predict, show_results

def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=load_model,
                inputs="params:weights_path",
                outputs="yolov5_loaded_model",
                name="load_yolov5_model"
            ),
            node(
                func=predict,
                inputs=dict(
                    model="yolov5_loaded_model",
                    image_path="params:image_path"  # Utilise 'params:image_path' du catalogue Kedro
                ),
                outputs="prediction_results",
                name="predict_with_yolov5"
            ),
            node(
                func=show_results,
                inputs=dict(
                    results="prediction_results",
                    image_path="params:image_path",
                    save_path="params:save_path"  # Sauvegarde l'image annotée
                ),
                outputs=None,
                name="show_prediction_results"
            ),
        ]
    )
