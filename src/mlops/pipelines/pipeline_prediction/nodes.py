"""
This is a boilerplate pipeline 'pipeline_prediction'
generated using Kedro 0.19.6
"""
#nodes.py

from yolov5 import detect
import torch
import matplotlib.pyplot as plt
import cv2

def load_model(weights_path):
    # Load the YOLOv5 model
    model = torch.hub.load('ultralytics/yolov5', 'custom', path=weights_path)
    return model

def predict(model, image_path):
    # Perform prediction using YOLOv5
    results = model(image_path)
    return results

def show_results(results, image_path,save_path):
    # Render the results on the image
    results.render()  # updates results.ims with boxes and labels
    img = results.ims[0]  # BGR image
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Convert to RGB

    # Save the image with detections
    plt.imsave(save_path, img)
