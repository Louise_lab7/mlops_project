from kedro.framework.hooks import hook_impl
from pyspark import SparkConf
from pyspark.sql import SparkSession
import logging
import os

log = logging.getLogger(__name__)

class SparkHooks:
    @hook_impl
    def after_context_created(self, context) -> None:
        try:
            # Vérifier si le fichier existe
            spark_conf_path = os.path.join(context.project_path, "conf", "base", "spark.yml")
            log.info("Vérification du chemin du fichier spark.yml: %s", spark_conf_path)
            if not os.path.exists(spark_conf_path):
                log.error("Le fichier spark.yml est introuvable à l'emplacement : %s", spark_conf_path)
                return
            else:
                log.info("Le fichier spark.yml existe à l'emplacement : %s", spark_conf_path)

            # Charge la configuration spark dans spark.yml
            parameters = context.config_loader.get("spark*", "spark*/**")
            log.info("Loaded raw spark parameters: %s", parameters)

            if not isinstance(parameters, dict) or 'spark' not in parameters:
                raise TypeError("Configuration 'spark' should be a dictionary containing 'spark' key.")

            spark_config = parameters['spark']
            log.info("Loaded spark configuration: %s", spark_config)

            # Aplatir le dictionnaire de configuration spark
            def flatten_dict(d, parent_key='', sep='.'):
                items = []
                for k, v in d.items():
                    new_key = f"{parent_key}{sep}{k}" if parent_key else k
                    if isinstance(v, dict):
                        items.extend(flatten_dict(v, new_key, sep=sep).items())
                    else:
                        items.append((new_key, v))
                return dict(items)

            flattened_spark_config = flatten_dict(spark_config)
            log.info("Flattened spark configuration: %s", flattened_spark_config)

            spark_conf = SparkConf().setAll(flattened_spark_config.items())

            # Initialize the spark session
            spark_session_conf = (
                SparkSession.builder.appName(context.project_path.name)
                .enableHiveSupport()
                .config(conf=spark_conf)
            )
            _spark_session = spark_session_conf.getOrCreate()
            _spark_session.sparkContext.setLogLevel("WARN")
        except Exception as e:
            log.error("An error occurred while initializing SparkSession: %s", e)
