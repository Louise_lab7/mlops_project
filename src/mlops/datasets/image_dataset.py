# src/mlops/datasets/image_dataset.py
import os
import numpy as np
from PIL import Image
from kedro.io import AbstractDataset

class ImageDataSet(AbstractDataset):
    def __init__(self, filepath):
        self.filepath = filepath

    def _load(self):
        images = []
        for filename in os.listdir(self.filepath):
            if filename.lower().endswith(('.png', '.jpg', '.jpeg', '.bmp', '.gif')):
                img_path = os.path.join(self.filepath, filename)
                img = Image.open(img_path)
                if img is not None:
                    images.append(np.array(img))
        return images

    def _save(self, data):
        raise NotImplementedError("Saving not implemented for ImageDataSet")

    def _describe(self):
        return dict(filepath=self.filepath)