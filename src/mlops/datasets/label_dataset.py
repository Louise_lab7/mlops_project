# src/mlops/datasets/label_dataset.py
import os
from kedro.io import AbstractDataset

class LabelDataSet(AbstractDataset):
    def __init__(self, filepath):
        self.filepath = filepath

    def _load(self):
        labels = []
        for filename in os.listdir(self.filepath):
            if filename.lower().endswith('.txt'):
                label_path = os.path.join(self.filepath, filename)
                with open(label_path, 'r') as file:
                    labels.append(file.read())
        return labels

    def _save(self, data):
        raise NotImplementedError("Saving not implemented for LabelDataSet")

    def _describe(self):
        return dict(filepath=self.filepath)